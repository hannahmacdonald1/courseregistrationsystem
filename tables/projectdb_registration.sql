CREATE DATABASE  IF NOT EXISTS `projectdb` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `projectdb`;
-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: localhost    Database: projectdb
-- ------------------------------------------------------
-- Server version	8.0.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `registration`
--

DROP TABLE IF EXISTS `registration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `registration` (
  `REG_ID` int NOT NULL auto_increment,
  `STUDENT_ID` int NOT NULL,
  `OFFERING_ID` int NOT NULL,
  `GRADE` varchar(3) NOT NULL,
  PRIMARY KEY (`REG_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `registration`
--

LOCK TABLES `registration` WRITE;
/*!40000 ALTER TABLE `registration` DISABLE KEYS */;
-- Adding each student individually
INSERT INTO `registration` VALUES (1,1000,1,'A+'),(2,1000,4,'A+'),(3,1000,3,'A+'),(4,1000,7,'A+'),(5,1000,8,'A+'),(6,1000,6,'A+');
INSERT INTO `registration` VALUES (7,1001,2,'B+'),(8,1001,3,'A+'),(9,1001,6,'A-'),(10,1001,7,'A+'),(11,1001,5,'A+'),(12,1001,8,'C+');
INSERT INTO `registration` VALUES (13,1002,1,'A+'),(14,1002,4,'A-'),(15,1002,3,'A'),(16,1002,7,'B-'),(17,1002,8,'B'),(18,1002,6,'B+');
INSERT INTO `registration` VALUES (19,1003,2,'B+'),(20,1003,3,'A+'),(21,1003,6,'A-'),(22,1003,7,'A+'),(23,1003,5,'A+'),(24,1003,8,'C+');
INSERT INTO `registration` VALUES (25,1004,1,'A+'),(26,1004,4,'A+'),(27,1004,3,'A+'),(28,1004,7,'A+'),(29,1004,8,'A+'),(30,1004,6,'A+');
INSERT INTO `registration` VALUES (31,1005,2,'B+'),(32,1005,3,'A+'),(33,1005,6,'A-'),(34,1005,7,'A+'),(35,1005,5,'A+'),(36,1005,8,'C+');
INSERT INTO `registration` VALUES (37,1006,1,'A+'),(38,1006,4,'A+'),(39,1006,3,'A+'),(40,1006,7,'A+'),(41,1006,8,'A+'),(42,1006,6,'A+');
INSERT INTO `registration` VALUES (43,1007,2,'B+'),(44,1007,3,'A+'),(45,1007,6,'A-'),(46,1007,7,'A+'),(47,1007,5,'A+'),(48,1007,8,'C+');
INSERT INTO `registration` VALUES (49,1008,1,'C'),(50,1008,4,'A+');
INSERT INTO `registration` VALUES (51,1009,2,'B+'),(52,1009,5,'B-');
INSERT INTO `registration` VALUES (53,1010,1,'C'),(54,1010,4,'A+');
INSERT INTO `registration` VALUES (55,1011,2,'B+'),(56,1011,5,'B-');
INSERT INTO `registration` VALUES (57,1012,1,'C'),(58,1012,4,'A+');
INSERT INTO `registration` VALUES (59,1013,2,'B+'),(60,1013,5,'B-');
INSERT INTO `registration` VALUES (61,1014,1,'C'),(62,1014,4,'A+');
INSERT INTO `registration` VALUES (63,1015,2,'B+'),(64,1015,5,'B-');
/*!40000 ALTER TABLE `registration` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-04-18 18:38:09
