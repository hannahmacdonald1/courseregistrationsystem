
package serverController;

import java.io.IOException;
import java.net.ServerSocket;
import java.sql.SQLException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import serverModel.CourseCatalogue;


/**
 * A class that represents a server in a client/server Course
 * Registration Application.
 *@author Brianna Gwilliam, Hannah MacDonald, Muskan Sarvesh
 *@since April 12, 2020
 *@version 2.0
 */
public class ServerComController {
    
    /**
     * The ServerSocket used by the server
     */
    private ServerSocket serverSocket;
    
    
    /**
     * The catalogue of courses used by the application
     */
    CourseCatalogue cat;
    
    
    /**
     * The threadpool used to support multiple clients
     */
    private ExecutorService pool;
    
    

    /**
     * Constructs a server with Port 8099
     * @author Brianna Gwilliam
     */
    public ServerComController() {
        try {
            cat = new CourseCatalogue();
            serverSocket = new ServerSocket(8099);
            pool = Executors.newCachedThreadPool();
        }
        
        catch(IOException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Accepts connections from new clients.
     * @author Brianna Gwilliam
     */
    public void communicate() {
        try {
            while(true) {
                ServerHelper serverHelper = new ServerHelper(serverSocket.accept(), cat);
                
                System.out.println("Accepted a client");
                
                pool.execute(serverHelper);
                
            }
        }
        
        catch(Exception e) {
            e.printStackTrace();
            pool.shutdown();
        }
        
        
    }

    /**
     * Run the Server.
     *
     * @param args is not used
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        ServerComController s = new ServerComController();
        System.out.println("Server is now running");
        s.communicate();
    }
}

