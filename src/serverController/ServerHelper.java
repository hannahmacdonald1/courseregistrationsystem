
package serverController;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.sql.SQLException;
import java.util.NoSuchElementException;

import serverModel.CourseCatalogue;
import serverModel.Student;


/**
 * A class that assists in the server's communication in a
 * client/server Course Registration Application.
 *@author Brianna Gwilliam, Hannah MacDonald, Muskan Sarvesh
 *@since April 18, 2020
 *@version 1.0
 */
public class ServerHelper implements Runnable {
    

    /**
     * The socket used by the client and server
     */
    private Socket aSocket;
    
    
    /**
     * Used for writing to the socket
     */
    private PrintWriter socketOutput;
    
    /**
     * Used for reading from the socket
     */
    private BufferedReader socketInput;
    
    
    /**
     * The Student registering in courses
     */
    private Student student;
    
    /**
     * The catalogue of all courses
     */
    private CourseCatalogue cat;
    
    

    /**
     * Construct a ServerHelper object with the specified socket and
     * that has the specified value for member variable cat
     * @param s the socket to be used
     * @param cat the new value for cat
     * @author Brianna Gwilliam
     */
    public ServerHelper(Socket s, CourseCatalogue cat) {
        try {
            student = new Student(null, 0);
            this.cat = cat;
            
            aSocket = s;
            socketInput = new BufferedReader(new InputStreamReader(aSocket.getInputStream()));
            socketOutput = new PrintWriter(aSocket.getOutputStream(), true);
        } catch (IOException e) {
        }
    }

    /**
     * Gets input from the Client and acts accordingly.
     * @author Brianna Gwilliam
     */
    public void run() {
        String line = null;
        String[] input = null;
        
        
        try {
        
        while (true) {
            
            
            
            line = socketInput.readLine();
            input = line.split(",");
            
            
            String name = null;
            int number = 0;
            int sec = 0;
        
            if(input[0].contentEquals("QUIT")) //quit condition
                break;
            
            switch(Integer.parseInt(input[0])) {
            case(1): // search catalogue
                name = input[1];
                number = Integer.parseInt(input[2]);
                socketOutput.println(cat.searchCatalogue(name, number));
                break;
                
            case(2): //register in a course
                name = input[1];
                number = Integer.parseInt(input[2]);
                sec = Integer.parseInt(input[3]);
                try {
                    student.setStudentRegList(cat.getDB().getStudentCourses(student.getStudentId()));
                    socketOutput.println(cat.addCourse(student, name, number, sec));
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                break;
            
            case(3): // remove student registration
                name = input[1];
                number = Integer.parseInt(input[2]);
                try {
                    student.setStudentRegList(cat.getDB().getStudentCourses(student.getStudentId()));
                    socketOutput.println(cat.getDB().removeRegistration(name, number,student));
                } catch (SQLException e2) {
                    e2.printStackTrace();
                }
                
                break;
            
            case(4): // view courses in catalogue
                socketOutput.println(cat);
                break;
            case(5): //view student registration
                // Get the registrations from database
                try {
                    student.setStudentRegList(cat.getDB().getStudentCourses(student.getStudentId()));
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                // Print them to the socket
                socketOutput.println(student.toString() + student.showAllRegisteredCourses());
                break;
                
            case(6): //set student information
                String studentName = null;
                try {
                    studentName = cat.getDB().createStudent(Integer.parseInt(input[2]),input[1]);
                } catch (SQLException e) {
                    e.printStackTrace();
                    System.exit(1);
                }
                // If the student entered their name different from what is in the database
                if (studentName!= null)
                    student.setStudentName(studentName);
                else
                    student.setStudentName(input[1]);
                
                student.setStudentId(Integer.parseInt(input[2]));
                
            }
            
            
        }
        
        socketInput.close();
        socketOutput.close();
            
        } catch(IOException e) {
            e.printStackTrace();
        }
        catch(NoSuchElementException e) {
            e.printStackTrace();
        }
        
        catch(NumberFormatException e) {
            e.printStackTrace();
        }
                
            
        
    }

}

