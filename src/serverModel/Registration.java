package serverModel;

/**
 * Handles student registrations in specific course offerings. Contains
 * the student's grade.
 * @author Brianna Gwilliam, Hannah MacDonald, Muskan Sarvesh
 * @since April 12, 2020
 * @version 2.0
 */
public class Registration {
    
    /**
     * The student who is registered in the offering
     */
	private Student theStudent;
    
    /**
     * The course offering that the student is registered in
     */
	private CourseOffering theOffering;
    
    /**
     * The student's grade in the course
     */
	private String grade;
	
	/**
	 * The registration's unique ID number
	 */
	private int regID;
	
	/**
	 * Default constructor for Registration
	 */
	public Registration() {};
	
	/**
	 * Creates a registration object
	 * @param theStudent is the student
	 * @param theOffering is the course offering
	 * @param grade is the student's grade in the course
	 * @param regID is the unique registration id number
	 */
	public Registration(Student theStudent, CourseOffering theOffering, String grade, int regID) {
		this.theStudent = theStudent;
		this.theOffering = theOffering;
		this.grade = grade;
		this.setRegID(regID);
	}
	
	/**
     * An exception that is thrown when the student is already enrolled in
     * the maximum number of courses (6)
     * @author Hannah MacDonald
     */
	@SuppressWarnings("serial")
	public class TooManyRegistrationsException extends Exception {
		public TooManyRegistrationsException() {
			super("Error! This student is enrolled in too many courses!");
		}
	}
	
	/**
	 * Throws a TooManyRegistrationsException. For use by the Database class.
	 * @throws TooManyRegistrationsException
	 */
	public void throwTMRException () throws TooManyRegistrationsException {
		throw new TooManyRegistrationsException();
	}
	
	/**
     * An exception that is thrown when the course offering is full
     * @author Hannah MacDonald
     */
	@SuppressWarnings("serial")
	public class SectionFullException extends Exception {
		public SectionFullException() {
			super("Error! The course section is full!");
		}
	}
	
	/**
	 * Throws a SectionFullException. For use by the Database class.
	 * @throws SectionFullException
	 */
	public void throwSFException() throws SectionFullException {
		throw new SectionFullException();
	}
	
	/*
	 * Getters and setters
	 */
	
    
    /**
     * Gets the student registered in the offering
     * @return theStudent
     */
	public Student getTheStudent() {
		return theStudent;
	}
    
    /**
    * Sets the student registered in the offering to the specified value
    * @param theStudent the new student
    */
	public void setTheStudent(Student theStudent) {
		this.theStudent = theStudent;
	}
    
    /**
    * Gets the offering that the student is registered in
    * @return theOffering
    */
	public CourseOffering getTheOffering() {
		return theOffering;
	}
    
    /**
       * Sets the offering that the student is registered in to the specified value
       * @param theOffering the new offering
       */
	public void setTheOffering(CourseOffering theOffering) {
		this.theOffering = theOffering;
	}
    
    /**
    * Gets the student's grade in the course
    * @return grade
    */
	public String getGrade() {
		return grade;
	}
    
    /**
    * Sets the student's grade in the course to the specified value
    * @param grade the new grade
    */
	public void setGrade(String grade) {
		this.grade = grade;
	}
	
	/**
	 * Sets the unique registration id for this registration
	 * @return the id number
	 */
	public int getRegID() {
		return regID;
	}

	/**
	 * Gets the unique registration id for this registration
	 * @param regID is the id number
	 */
	public void setRegID(int regID) {
		this.regID = regID;
	}

	@Override
    /**
     * Creates a String representation of a Registration object
     * @return the String representation
     */
	public String toString () {
		String st = "";
		st += getTheOffering ();
		st += "Grade: " + getGrade();
		st += "\n-----------\n";
		return st;
		
	}
	

}
