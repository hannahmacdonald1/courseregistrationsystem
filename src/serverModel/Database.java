package serverModel;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import serverModel.Registration.SectionFullException;
import serverModel.Registration.TooManyRegistrationsException;
import serverModel.Student.AlreadyEnrolledException;

import java.sql.*;

/**
 * Connects to the database and performs all necessary actions and updates
 * @author Hannah MacDonald
 *
 */
public class Database implements DBCredentials {

    /**
     * The connection to the database
     */
    private Connection conn;
    /**
     * The primary result set
     */
    private ResultSet rs;
    /**
     * The secondary result set
     */
    private ResultSet supplementaryrs;
    
    /**
     * Constructor that connects to the database
     * @author Hannah MacDonald
     */
    public Database() {
        try {
            initializeConnection();
        } catch (SQLException e) {
            System.err.println("Failed to connect to database, server could not start.");
            System.exit(1);
        }
    }
    
    /**
     * Connects to the database.
     * @author Hannah MacDonald
     * @throws SQLException when connection could not be established
     */
    private void initializeConnection() throws SQLException {
        // Register JDBC driver
        Driver driver = new com.mysql.cj.jdbc.Driver();
        DriverManager.registerDriver(driver);
        // Open a connection
        conn = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);
    }
    
    /**
     * Creates a student from their name and id, if it doesn't already exist, and inserts it into the database.
     * @author Hannah MacDonald
     * @param id is the student ID
     * @param Name is the student name
     * @return the name of the student if it differs in the database, or null otherwise
     * @throws SQLException when a database error occurs
     */
    public String createStudent(int id, String Name) throws SQLException {
        try {
            boolean exists = studentExists(id,Name);
            if (exists)
                return null;
        } catch (NameDiffersException e) {
            String q = "SELECT NAME FROM STUDENT WHERE ID="+id;
            Statement stat = conn.prepareStatement(q);
            rs = stat.executeQuery(q);
            rs.next();
            return rs.getString("NAME");
        }
        
        String query = "INSERT INTO STUDENT (ID,name,reg1,reg2,reg3,reg4,reg5,reg6) values(?,?,null,null,null,null,null,null)";
        PreparedStatement pStat = conn.prepareStatement(query);
        pStat.setInt(1, id);
        pStat.setString(2, Name);
        pStat.executeUpdate();
        pStat.close();
        return null;
    }
    
    /**
     * Determines whether a student exists in the database already
     * @author Hannah MacDonald
     * @param id is the student ID
     * @param Name is the student name
     * @return true if student exists, false otherwise
     * @throws NameDiffersException if the student exists and their name is different in the database than the one provided
     * @throws SQLException when a database error occurs
     */
    private boolean studentExists(int id, String Name) throws SQLException, NameDiffersException {
        String query = "SELECT * FROM STUDENT WHERE ID="+id;
        Statement stat = conn.prepareStatement(query);
        rs = stat.executeQuery(query);
        
        while(rs.next()) {
            if(rs.getString("NAME").equalsIgnoreCase(Name))
                return true;
            throw new NameDiffersException();
        }
        
        return false;
    }
    
    /**
     * An exception that is thrown when the name given for student
     * does not match their name in the database
     * @author Hannah MacDonald
     */
    @SuppressWarnings("serial")
    public class NameDiffersException extends Exception {
        public NameDiffersException() {
            super("Wrong name!");
        }
    }
    
    /*
     * Add course functions
     */
    
    /**
     * Creates a registration for a student in the specified course section.
     * @author Hannah MacDonald
     * @param theStudent is the student
     * @param offering is the courseOffering
     * @throws TooManyRegistrationsException if the student is enrolled in 6 or more courses
     * @throws SQLException when a database error occurs
     * @throws SectionFullException if the course section is full
     * @throws AlreadyEnrolledException if the student is already enrolled
     */
    synchronized public void createRegistration(Student theStudent, CourseOffering offering) throws TooManyRegistrationsException, SQLException, SectionFullException, AlreadyEnrolledException {
        Registration r = new Registration(theStudent,offering,"-", numberOfRegistrations() +1);
        
        if (numberOfRegistrations(theStudent) >= 6) {
            r.throwTMRException();
        }
        
        if (offering.getSecCap() == numberOfRegistrations(offering)) {
            r.throwSFException();
        }
        
        if (isEnrolled(theStudent,offering)) {
            theStudent.throwAEException();
        }
        
        addRegistration(theStudent,offering);
        
    }
    
    /**
     * Checks whether a student is already enrolled in a course
     * @param theStudent is the student
     * @param offering is the course offering
     * @return true if the student is enrolled and false otherwise
     * @throws SQLException when a database error occurs
     */
    public boolean isEnrolled(Student theStudent, CourseOffering offering) throws SQLException {
        
        String query = "SELECT * FROM COURSEOFFERING WHERE COURSE_ID="+offering.getTheCourse().getCourseID();
        Statement stat = conn.prepareStatement(query);
        rs = stat.executeQuery(query);
        
        while(rs.next()) {
            query = "SELECT * FROM REGISTRATION WHERE OFFERING_ID="+rs.getInt("OFFERING_ID");
            stat = conn.prepareStatement(query);
            supplementaryrs = stat.executeQuery(query);
            while (supplementaryrs.next()) {
                if(supplementaryrs.getInt("STUDENT_ID")==theStudent.getStudentId())
                    return true;
            }
        }
        
        return false;
    }
    
    /**
     * Creates a registration in the registration table
     * @author Hannah MacDonald
     * @param theStudent is the student
     * @param offering is the course section
     * @throws SQLException when a database error occurs
     */
    private void addRegistration(Student theStudent, CourseOffering offering) throws SQLException {
        //Add to registration list
        String query = "INSERT INTO REGISTRATION (REG_ID,STUDENT_ID,OFFERING_ID,GRADE) values(?,?,?,'-')";
        PreparedStatement pStat = conn.prepareStatement(query);
        int regId = numberOfRegistrations()+1;
        pStat.setInt(1, regId);
        pStat.setInt(2, theStudent.getStudentId());
        pStat.setInt(3, offering.getOfferingID());
        pStat.executeUpdate();
        
        createStudentRegistration(theStudent,regId);
        
    }
    
    /**
     * Adds a created registration to a students' list of registered courses
     * @author Hannah MacDonald
     * @param theStudent is the student
     * @param regID is the unique registration id number
     * @throws SQLException when a database error occurs
     */
    private void createStudentRegistration(Student theStudent, int regID) throws SQLException {
        String query = "SELECT * FROM STUDENT WHERE ID="+theStudent.getStudentId();
        Statement stat = conn.prepareStatement(query);
        rs = stat.executeQuery(query);
        while(rs.next()) {
            for (int i = 1; i < 7; i++) {
                String column = "REG" + i;
                int reg = rs.getInt(column);
                if (reg == 0) {
                    query = "UPDATE STUDENT SET "+ column + "=" + regID +" WHERE ID="+theStudent.getStudentId();
                    PreparedStatement pStat = conn.prepareStatement(query);
                    pStat.executeUpdate();
                    return;
                }
            }
        }
    }
    
    /**
     * Gets the number of registrations in a course offering
     * @author Hannah MacDonald
     * @param offering is the course offering
     * @return the number of registrations in a given course offering
     * @throws SQLException when a database error occurs
     */
    private int numberOfRegistrations(CourseOffering offering) throws SQLException {
        int count = 0;
        String query = "SELECT * FROM REGISTRATION WHERE OFFERING_ID="+offering.getOfferingID();
        Statement stat = conn.prepareStatement(query);
        rs = stat.executeQuery(query);
        while (rs.next()) {
            count++;
        }
        return count;
    }
    
    /**
     * Gets the total number of registrations
     * @author Hannah MacDonald
     * @return the number of registrations
     * @throws SQLException when a database error occurs
     */
    private int numberOfRegistrations() throws SQLException {
        int count = 0;
        String query = "SELECT * FROM REGISTRATION";
        Statement stat = conn.prepareStatement(query);
        rs = stat.executeQuery(query);
        while (rs.next()) {
            count++;
        }
        return count;
    }
    
    /**
     * Gets the number of courses that a student is in
     * @param theStudent is the student
     * @return the number of courses the student is registered in
     * @throws SQLException when a database error occurs
     */
    private int numberOfRegistrations (Student theStudent) throws SQLException {
        int count = 0;
        String query = "SELECT * FROM STUDENT WHERE ID="+theStudent.getStudentId();
        Statement stat = conn.prepareStatement(query);
        rs = stat.executeQuery(query);
        while(rs.next()) {
        for (int i = 1; i < 7; i++) {
            String column = "REG" + i;
            int reg = rs.getInt(column);
            if (reg!= 0)
                count++;
        }
        }
        return count;
    }
    
    /**
     * Gets a list of all the courses that the student is registered in
     * @author Hannah MacDonald
     * @param id is the student ID
     * @return an array list containing all of the student's registrations
     * @throws SQLException when a database error occurs
     */
    public ArrayList<Registration> getStudentCourses(int id) throws SQLException {
        ArrayList<Registration> regs = new ArrayList<Registration>();
        
        String query = "SELECT * FROM STUDENT WHERE ID="+id;
        Statement stat = conn.prepareStatement(query);
        supplementaryrs = stat.executeQuery(query);
        supplementaryrs.next();
        
        String studentName = supplementaryrs.getString("NAME");
        Student theStudent = new Student(studentName, id);
        
        for (int i = 1; i < 7; i++) {
            String column = "REG" + i;
            int reg = supplementaryrs.getInt(column);
            if (reg != 0) { // there is a valid registration
                query = "SELECT * FROM REGISTRATION WHERE REG_ID="+reg;
                stat = conn.prepareStatement(query);
                rs = stat.executeQuery(query);
                rs.next();
                String grade = rs.getString("GRADE");
                int offID = rs.getInt("OFFERING_ID");
                
                query = "SELECT * FROM COURSEOFFERING WHERE OFFERING_ID="+offID;
                stat = conn.prepareStatement(query);
                rs = stat.executeQuery(query);
                rs.next();
                int secNum = rs.getInt("SEC_NUM");
                int capacity = rs.getInt("CAPACITY");
                CourseOffering offering = new CourseOffering(secNum,capacity, rs.getInt("OFFERING_ID"));
                int courseID = rs.getInt("COURSE_ID");
                
                query = "SELECT * FROM COURSE WHERE COURSE_ID="+courseID;
                stat = conn.prepareStatement(query);
                rs = stat.executeQuery(query);
                rs.next();
                String name = rs.getString("COURSE_NAME");
                int num = rs.getInt("COURSE_NUM");
                int courseId =  rs.getInt("COURSE_ID");
                Course theCourse = new Course(name,num,courseId);
                
                offering.setTheCourse(theCourse);
                Registration registration = new Registration(theStudent,offering,grade,reg);
                regs.add(registration);
            }
        }
        return regs;
    }

    /**
     * Finds and constructs a course from the catalogue.
     * @author Hannah MacDonald
     * @param courseName is the course name
     * @param courseNum is the course number
     * @return the course if found, or null otherwise
     * @throws SQLException if there are any errors with the database
     */
    public Course searchCatalogue (String courseName, int courseNum) throws SQLException {
        
        Course theCourse = null; // if not found
        String query = "SELECT * FROM COURSE WHERE COURSE_NAME='"+courseName+"'";
        Statement stat = conn.prepareStatement(query);
        rs = stat.executeQuery(query);
        
        while(rs.next()) {
            if (rs.getInt("COURSE_NUM") == courseNum) {
                int courseID = rs.getInt("COURSE_ID");
                theCourse = new Course(courseName,courseNum,courseID);
                
                // Add prerequisites
                for (int i = 1; i < 4; i++) {
                    String column = "PREREQ" + i;
                    int preReq = rs.getInt(column);
                    if (preReq != 0) { // add check for uninitialized preReqs to stuff that uses them
                        theCourse.setPreReqIndexAt(i-1,preReq);
                        
                        query = "SELECT COURSE_NAME, COURSE_NUM FROM COURSE WHERE COURSE_ID="+preReq;
                        stat = conn.prepareStatement(query);
                        supplementaryrs = stat.executeQuery(query);
                        
                        Course preReqCourse = new Course(supplementaryrs.getString("COURSE_NAME"),supplementaryrs.getInt("COURSE_NUM"),supplementaryrs.getInt("COURSE_ID"));
                        theCourse.addPrerequisite(preReqCourse);
                    }
                }
            
                // Add offerings
                query = "SELECT * FROM COURSEOFFERING WHERE COURSE_ID="+courseID;
                stat = conn.prepareStatement(query);
                supplementaryrs = stat.executeQuery(query);
                
                while(supplementaryrs.next()) {
                    int secNum = supplementaryrs.getInt("SEC_NUM");
                    int capacity = supplementaryrs.getInt("CAPACITY");
                    CourseOffering offering = new CourseOffering(secNum,capacity,supplementaryrs.getInt("OFFERING_ID"));
                    offering.setTheCourse(theCourse);
                    theCourse.addOffering(offering);
                }
                
                break;
            }
        }
        
        return theCourse;
    }
    
    /**
     * Fetches all the courses in the database. Used for printing
     * all the courses in the catalogue.
     * @author Hannah MacDonald
     * @return an array list containing all the courses
     * @throws SQLException when there is an error reading from the database
     */
    public ArrayList<Course> getAllCourses () throws SQLException {
        
        ArrayList<Course> theCourses = new ArrayList<Course>();
        
        String query = "SELECT * from COURSE";
        Statement stat = conn.prepareStatement(query);
        supplementaryrs = stat.executeQuery(query);
        
        while(supplementaryrs.next()) {
            // Get general course information
            String name = supplementaryrs.getString("COURSE_NAME");
            int num = supplementaryrs.getInt("COURSE_NUM");
            int [] preReq = new int[3];
            preReq[0] = supplementaryrs.getInt("PREREQ1");
            preReq[1] = supplementaryrs.getInt("PREREQ2");
            preReq[2] = supplementaryrs.getInt("PREREQ3");
            int ID =  supplementaryrs.getInt("COURSE_ID");
            Course thisCourse = new Course(name,num,preReq,ID);
            
            // Find the offerings
            String queryA = "SELECT OFFERING_ID, SEC_NUM, CAPACITY from COURSEOFFERING WHERE COURSE_ID="+ID;
            Statement statA = conn.prepareStatement(queryA);
            rs = statA.executeQuery(queryA);
            
            while (rs.next()) {
                 int secNum = rs.getInt("SEC_NUM");
                 int capacity = rs.getInt("CAPACITY");
                 CourseOffering offering = new CourseOffering(secNum,capacity, rs.getInt("OFFERING_ID"));
                 offering.setTheCourse(thisCourse);
                 thisCourse.addOffering(offering);
            }
            rs.close();
            theCourses.add(thisCourse);
        }
        supplementaryrs.close();
        
        //Fix prerequisites
        for (int i = 0; i < theCourses.size(); i++) {
            for (int j = 0; j < theCourses.get(i).preReqIndexSize(); j++) {
                int index = theCourses.get(i).getPreReqIndexAt(j);
                if(index != 0)
                    theCourses.get(i).addPrerequisite(theCourses.get(index));
            }
            theCourses.get(i).setPreReqIndex(null);
        }
        
        return theCourses;
        
    }
    
    /*
     * Remove course methods
     */
    
    /**
     * Removes a student from a course.
     * @author Hannah MacDonald
     * @param courseName is the course name
     * @param courseNum is the course number
     * @param theStudent is the student
     * @return null if successful or the error otherwise
     * @throws SQLException when a database error occurs
     */
    synchronized public String removeRegistration(String courseName, int courseNum, Student theStudent) throws SQLException {
        ArrayList<Registration> currentRegs = getStudentCourses(theStudent.getStudentId());
        
        //Look for a registration that matches the course name and number
        int count = 1;
        for (Registration r: currentRegs) {
            if (courseName.equals(r.getTheOffering().getTheCourse().getCourseName()) &&
                    courseNum == r.getTheOffering().getTheCourse().getCourseNum()) {
                boolean success = removeRegistrationFromTable(r.getRegID(),r.getTheOffering());
                if (!success) {
                    return "notenough";
                }
                //remove from student
                removeRegistrationFromStudent(count,theStudent.getStudentId());
                return null;
            }
            count++;
        }
        return "notfound";
    }
    
    /**
     * Removes a course from a student's list of registrations
     * @author Hannah MacDonald
     * @param index is the reg column number that the registration is under in the Student Table
     * @param studentID is the student ID
     * @throws SQLException when a database error occurs
     */
    private void removeRegistrationFromStudent(int index, int studentID) throws SQLException {
        String column = "REG"+index;
        String query = "UPDATE STUDENT SET "+ column + "=null WHERE ID="+studentID;
        PreparedStatement pStat = conn.prepareStatement(query);
        pStat.executeUpdate();
    }
    
    /**
     * Removes a student from the registration table.
     * @author Hannah MacDonald
     * @param regID is the registration id
     * @param offering is the course offering
     * @return false if the student cannot be removed and true otherwise
     * @throws SQLException when a database error occurs
     */
    private boolean removeRegistrationFromTable(int regID, CourseOffering offering) throws SQLException {
        if (numberOfRegistrations(offering) <= 8) // Each course must have at least 8 registrations
            return false;
        
        String query = "DELETE FROM REGISTRATION WHERE REG_ID="+regID;
        Statement stat = conn.prepareStatement(query);
        stat.executeUpdate(query);
        return true;
    }
    
}
