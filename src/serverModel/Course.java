package serverModel;
import java.util.ArrayList;

/**
 * Represents a course.
 * Contains a list of prerequisites and course sections, as well as the course name and number.
 * @author Brianna Gwilliam, Hannah MacDonald, Muskan Sarvesh
 * @since April 12, 2020
 * @version 2.0
 */
public class Course {
	
	/**
	 * The course's unique id number
	 */
	private int courseID;

    /**
     * The name of the course
     */
	private String courseName;
    
    /**
     * The number of the course
     */
	private int courseNum;
    
    /**
     * The prerequisite(s) for the course
     */
	private ArrayList<Course> preReq;
	
	/**
	 * Used by the database when initializing a course object
	 */
	private int [] preReqIndex;
    
    /**
     * The section(s) available for the course
     */
	private ArrayList<CourseOffering> offeringList;

    
    /**
     * Constructs a Course object with the specified values for member variables
     * courseName and courseNum
     * @param courseName the course's name
     * @param courseNum the course's number
     * @param courseID is this course's unique course id
     */
	public Course(String courseName, int courseNum, int courseID) {
		this.setCourseName(courseName);
		this.setCourseNum(courseNum);
		this.setCourseID(courseID);
		
		preReq = new ArrayList<Course>();
		offeringList = new ArrayList<CourseOffering>();
		preReqIndex = null;
	}
	
	/**
	 * Creates a course with a list of prerequisite indices. Used by the Database class
	 * to construct a proper course
	 * @param courseName is the course name
	 * @param courseNum is the course number
	 * @param preReqIndex is an array of courseIDs for prerequisites
	 * @param courseID is this course's unique course id
	 */
	public Course (String courseName, int courseNum, int [] preReqIndex, int courseID) {
		this.setCourseName(courseName);
		this.setCourseNum(courseNum);
		this.setCourseID(courseID);
		this.preReqIndex = preReqIndex;
		preReq = new ArrayList<Course>();
		offeringList = new ArrayList<CourseOffering>();
	}
	
	/**
	 * Adds an offering (section) to the course
	 * @author Hannah MacDonald
	 * @param offering is the course offering
	 */
	public void addOffering(CourseOffering offering) {
		offeringList.add(offering);
	}
	
	/*
	 * Getters and setters
	 */

    /**
     * Gets the name of the course
     * @return courseName
     */
	public String getCourseName() {
		return courseName;
	}

    /**
    * Sets the name of the course to the specified value
    * @param courseName the new name of the course
    */
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

    /**
    * Gets the number of the course
    * @return courseNum
    */
	public int getCourseNum() {
		return courseNum;
	}

    /**
    * Sets the number of the course to the specified value
    * @param courseNum the new course number
    */
	public void setCourseNum(int courseNum) {
		this.courseNum = courseNum;
	}
	
	/**
	 * Gets the unique course id for this course
	 * @return the id number
	 */
	public int getCourseID() {
		return courseID;
	}

	/**
	 * Sets the unique course id for this course
	 * @param courseID is the id number
	 */
	public void setCourseID(int courseID) {
		this.courseID = courseID;
	}

	@Override
    /**
     * Creates a String representation of a Course object
     * @return the String representation
     */
	public String toString () {
		String st = "\n";
		st += getCourseName() + " " + getCourseNum ();
		st += listPrequisiteCourses();
		st += "\nAll course sections:\n";
		for (CourseOffering c : offeringList)
			st += c;
		st += "\n-------";
		return st;
	}

	/**
	 * Finds a course offering at the specific index in the offering list
	 * @author Hannah MacDonald
	 * @param i is the index
	 * @return the course offering at that index, or null if it doesn't exist
	 */
	public CourseOffering getCourseOfferingAt(int i) {
		if (i < 0 || i >= offeringList.size() )
			return null;
		else
			return offeringList.get(i);
	}
	
	/**
	 * Finds the position of the course offering in the offering list
	 * @author Hannah MacDonald
	 * @param secNum is the section number
	 * @return the index of the offering, or -1 if not found.
	 */
	public int findCourseOffering(int secNum ) {
		for(int i = 0; i < offeringList.size(); i++) {
			if(offeringList.get(i).getSecNum()==secNum) {
				return i;
			}
		}
		return -1;
	}
	
	/**
	 * Adds a prerequisite course to the list
	 * @author Hannah MacDonald
	 * @param prereq is the prerequisite course
	 */
	public void addPrerequisite(Course prereq) {
		if (prereq!=null && !preReq.contains(prereq) ) {
			preReq.add(prereq);
		}
	}
	
	/**
	 * Gets the number of prerequisite courses
	 * @author Hannah MacDonald
	 * @return the number of prerequisites
	 */
	public int getNumberofPrerequisites() {
		return preReq.size();
	}
	
	/**
	 * Finds the prerequisite at an index in the list
	 * @author Hannah MacDonald
	 * @param index is the index
	 * @return the prerequisite at that index
	 */
	public Course getPrereqAt(int index) {
		return preReq.get(index);
	}
	
	/**
	 * Sets the prerequisite index array
	 * @param array in the array of prerequisite indices
	 */
	public void setPreReqIndex (int [] array) {
		preReqIndex = array;
	}
	
	/**
	 * Finds the prerequisite index at an index in the array
	 * @param index is the index
	 * @return 0 if the index is out of range or if the array is null, or the value at that index otherwise
	 */
	public int getPreReqIndexAt (int index) {
		if (preReqIndex == null || index > preReqIndex.length - 1 || index < 0)
			return 0;
		return preReqIndex[index];
	}
	
	/**
	 * Sets the prerequisite index at an index in the array
	 * @param index is the index
	 * @param value is the value to set at that index
	 */
	public void setPreReqIndexAt (int index, int value) {
		if (preReqIndex == null || index > preReqIndex.length - 1 || index < 0)
			return;
		preReqIndex[index] = value;
	}
	
	/**
	 * Gets the size of the prerequisite index array
	 * @return 0 if the array is null, or the size otherwise
	 */
	public int preReqIndexSize () {
		if (preReqIndex == null)
			return 0;
		return preReqIndex.length;
	}
	
	/**
	 * Lists all of the prerequisite courses for this course
	 * @author Hannah MacDonald
	 * @return a string showing all of the prerequisite courses.
	 */
	public String listPrequisiteCourses() {
		String st;
		st = "\nPrerequistes:\n";
		boolean added = false;
		for (int i = 0; i < getNumberofPrerequisites(); i++) {
			added = true;
			st += getPrereqAt(i).getCourseName() + " " + getPrereqAt(i).getCourseNum () + "\n";
		}
		if (!added)
			st += "None\n";
		return st;
	}

}

