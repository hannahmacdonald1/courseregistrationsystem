package serverModel;
import java.util.ArrayList;

/**
 * Represents the Student. Contains their name, student id and the courses
 * they are registered in.
 * @author Brianna Gwilliam, Hannah MacDonald, Muskan Sarvesh
 * @since April 12, 2020
 * @version 2.0
 */
public class Student {
	
    /**
     * The student's name
     */
	private String studentName;
    
    /**
     * The student's ID number
     */
	private int studentId;
    
    /**
     * A list of the student's registrations
     */
	private ArrayList<Registration> studentRegList;
	
    
    /**
     * Constructs a new Student object with the specified values for studentName
     * and studentId
     * @author Hannah MacDonald
     * @param studentName the name of the student
     * @param studentId the ID number of the student
     */
	public Student (String studentName, int studentId) {
		this.setStudentName(studentName);
		this.setStudentId(studentId);
		studentRegList = new ArrayList<Registration>();
	}
		
	/**
     * An exception that is thrown when the student cannot be removed
     * from a course because it has 8 or fewer registrations
     * @author Hannah MacDonald
     */
	@SuppressWarnings("serial")
	public class NotEnoughRegistrationsException extends Exception {
		public NotEnoughRegistrationsException() {
			super("Error! Course offering has 8 or fewer registrations.\nThe student cannot be unenrolled.\n");
		}
	}
	
	/**
     * An exception that is thrown when the student is not in the course
     * that was searched for (to delete it)
     * @author Hannah MacDonald
     */
	@SuppressWarnings("serial")
	public class StudentCourseNotFoundException extends Exception {
		public StudentCourseNotFoundException() {
			super("Error! Course was not found in student's registration list!");
		}
	}
	
	/**
     * An exception that is thrown when the student is already enrolled in
     * a section of the course
     * @author Hannah MacDonald
     */
	@SuppressWarnings("serial")
	public class AlreadyEnrolledException extends Exception {
		public AlreadyEnrolledException() {
			super("Error! Student is already enrolled in this course!");
		}
	}
	
	/**
	 * Throws AlreadyEnrolledException. For use by the Database class.
	 * @throws AlreadyEnrolledException
	 */
	public void throwAEException() throws AlreadyEnrolledException {
		throw new AlreadyEnrolledException();
	}
	
	/*
	 * Getters and Setters
	 */
	
	
    /**
     * Gets the student's name
     * @return studentName
     * @author Hannah MacDonald
     */
	public String getStudentName() {
		return studentName;
	}

    /**
    * Sets the student's name to the specified value
    * @param studentName the new student name
     * @author Hannah MacDonald
    */
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

    /**
    * Gets the student's ID number
    * @return studentId
     * @author Hannah MacDonald
    */
	public int getStudentId() {
		return studentId;
	}

    /**
    * Sets the student's ID number to the specified value
    * @param studentId the student's new ID number
     * @author Hannah MacDonald
    */
	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}
    
    
	@Override
    /**
     * Creates a String representation of a Student object
     * @return the String representation
     * @author Hannah MacDonald
     */
	public String toString () {
		String st = "Student Name: " + getStudentName() + "\n" +
				"Student Id: " + getStudentId() + "\n\n";
		return st;
	}
	
	/**
	 * Sets the list of courses that a student is registered in
	 * @param regs is the array list of registrations
	 */
	public void setStudentRegList(ArrayList<Registration> regs) {
		studentRegList = regs;
	}
	
	/**
	 * Gets the number of courses the student is in.
	 * @author Hannah MacDonald
	 * @return the number of courses the student is registered in
	 */
	public int getNumberOfRegistrations( ) {
		return studentRegList.size();
	}
	
	/**
	 * Shows the courses that a student is currently registered in
	 * @author Hannah MacDonald
	 * @return a string containing all the registered courses.
	 */
	public String showAllRegisteredCourses() {
		String st = "";
		if(studentRegList.size() > 0) {
			for (Registration r : studentRegList) {
				st += r;
			}
			st += "\0";
		}
		else
			st = "is not enrolled in any courses.\n\0";
		return st;
	}
	

}
