package serverModel;
import java.sql.SQLException;
import java.util.ArrayList;

import serverModel.Registration.SectionFullException;
import serverModel.Registration.TooManyRegistrationsException;
import serverModel.Student.AlreadyEnrolledException;

/**
 * Represents all the courses that are being offered.
 * Contains a list of courses.
 * @author Brianna Gwilliam, Hannah MacDonald, Muskan Sarvesh
 * @since April 12, 2020
 * @version 2.0
 */
public class CourseCatalogue {
	
	/**
	 * A connection to the database
	 */
	private Database DB;
	
    /**
     * The list of all courses
     */
	private ArrayList <Course> courseList;
	
    /**
     * Constructs a new CourseCatalogue object by invoking the loadFromDataBase() method
     * @throws SQLException when connection cannot be established or course cannot be loaded
     */
	public CourseCatalogue () throws SQLException {
		DB = new Database();
		loadFromDatabase();
	}
	
	/**
	 * Loads data from our simulated database.
	 * @author Hannah MacDonald
	 * @throws SQLException when the courses cannot be loaded
	 */
	public void loadFromDatabase() throws SQLException {
		setCourseList(DB.getAllCourses());
	}
	
	/**
	 * Creates a course offering and adds it to the existing course
	 * @author Hannah MacDonald
	 * @param c is the course
	 * @param secNum is the section number for the course offering
	 * @param secCap is the maximum number of students in the course
	 * @param offeringID is the unique course offering id
	 */
	public void createCourseOffering (Course c, int secNum, int secCap, int offeringID) {
		if (c!= null) {
			CourseOffering theOffering = new CourseOffering (secNum, secCap, offeringID);
			c.addOffering(theOffering);
		}
	}
	
	
	/**
	 * Adds a course to a students' courses
	 * @author Hannah MacDonald
	 * @param theStudent is the student
	 * @param name is the course name
	 * @param courseNum is the course number
	 * @param courseSec is the course section
     * @return null if the course was added successfully, "offering" if the section does not exist,
     *     "course" if the course is not in the catalogue, "registrations" if the student has too many registrations.
     *     "sectionfull" if the section is full, and "alreadyenrolled" if the student is already registered
     *     for the course
	 * @throws SQLException if a database error occurs
	 */
	public String addCourse(Student theStudent, String name, int courseNum, int courseSec) throws SQLException  {
		
		try {
		
		Course toAdd;
		toAdd = searchCat(name, courseNum);
		int index;

		 if ((index = toAdd.findCourseOffering(courseSec))!= -1 ) {
			CourseOffering offering = toAdd.getCourseOfferingAt(index);
			Registration myReg = new Registration();
			DB.createRegistration(theStudent, offering);
			return null;
		} 
		return "offering";	
		
		}
		
		catch(CourseNotFoundException e) {
			return "course";
		}
		
		catch(TooManyRegistrationsException e) {
			return "registrations";
		}
		
		catch(SectionFullException e) {
			return "sectionfull";
		}
		
		catch(AlreadyEnrolledException e) {
			return "alreadyenrolled";
		}
	}
	
	/**
	 * Searches the catalogue for a course.
	 * @author Hannah MacDonald
	 * @param courseName is the course
	 * @param courseNum is the course number
	 * @return a string representing the course and all its offerings
	 */
	public String searchCatalogue (String courseName, int courseNum) {
		
		try {
			return searchCat(courseName, courseNum).toString();
		} 
		catch(CourseNotFoundException e) {
			return "-------"; 
		}
		catch (SQLException e) {
			return "-------"; 
		} 
	}
	
	/**
	 * Helper method that finds a course.
	 * @author Hannah MacDonald
	 * @param courseName is the course name
	 * @param courseNum is the course number
	 * @return the course
	 * @throws CourseNotFoundException if the course is not in the database
	 * @throws SQLException if a database error occurs
	 */
	private Course searchCat (String courseName, int courseNum) throws CourseNotFoundException, SQLException {
		Course theCourse = DB.searchCatalogue(courseName, courseNum);
		if (theCourse == null)
			throw new CourseNotFoundException();	
		return theCourse;
	}

	 /**
     * An exception that is thrown when the course is not found
     * in the database.
     * @author Hannah MacDonald
     */
	@SuppressWarnings("serial")
	public class CourseNotFoundException extends Exception {
		public CourseNotFoundException() {
			super("Course was not found!");
		}
	}
	
	 /**
     * An exception that is thrown when the course offering is not
     * found in the database
     * @author Hannah MacDonald
     */
	@SuppressWarnings("serial")
	public class OfferingNotFoundException extends Exception {
		public OfferingNotFoundException() {
			super("Course section was not found!");
		}
	}
	
    /**
     * Gets the ArrayList containing all courses
     * @return courseList
     */
	public ArrayList <Course> getCourseList() {
		return courseList;
	}

    /**
    * Sets the ArrayList containing all courses to the specfied value
    * @param courseList the new list of courses
    */
	public void setCourseList(ArrayList <Course> courseList) {
		this.courseList = courseList;
	}
	
	/**
	 * Gets the reference to the database
	 * @return the database reference
	 */
	public Database getDB() {
		return DB;
	}
    
	@Override
    /**
     * Creates a String representation of a CourseCatalogue object.
     * @return the String representation
     */
	public String toString () {
		String st = "All courses in the catalogue: \n";
		for (Course c : courseList) {
			st += c;  //This line invokes the toString() method of Course
			st += "\n";
		}
		return st + "\0";
	}

}

