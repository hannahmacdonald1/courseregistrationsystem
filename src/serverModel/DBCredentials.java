package serverModel;

/**
 * Contains constants to initialize the database
 * @author Hannah MacDonald
 */
public interface DBCredentials {
	   // JDBC driver name and database URL
	   static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";  
	   static final String DB_URL = "jdbc:mysql://localhost/projectdb";

	   //  Database credentials
	   static final String USERNAME = "root";
	   static final String PASSWORD = "ensf409";
}
