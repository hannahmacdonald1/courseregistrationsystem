package serverModel;
import java.util.ArrayList;

/**
 * Represents a course section. Has a cap on the number of students,
 * as well as a list of registrations
 * @author Brianna Gwilliam, Hannah MacDonald, Muskan Sarvesh
 * @since April 12, 2020
 * @version 2.0
 */
public class CourseOffering {
	
	private int offeringID;
	
    /**
     * The section number
     */
	private int secNum;
    
    /**
     * The maximum number of students in the section
     */
	private int secCap;
    
    /**
     * The course being offered
     */
	private Course theCourse;
    
    /**
     * A list of registrations for the course offering
     */
	private ArrayList <Registration> offeringRegList;
	
    /**
     * Constructs a CourseOffering object with the specified values for secNum and secCap, and the correct id number
     * @param secNum the value for member variable secNum
     * @param secCap the value for member variable secCap
     * @param offeringID is the unique id number for this offering
     */
	public CourseOffering (int secNum, int secCap, int offeringID) {
		this.setSecNum(secNum);
		this.setSecCap(secCap);
		this.setOfferingID(offeringID);
		offeringRegList = new ArrayList <Registration>();
	}
	
	/*
	 * Getters and setters
	 */
	
    /**
     * Gets the section number
     * @return secNum
     */
	public int getSecNum() {
		return secNum;
	}
    
    /**
    * Sets the section number to the specified value
    * @param secNum the new section number
    */
	public void setSecNum(int secNum) {
		this.secNum = secNum;
	}
    
    /**
    * Gets the section capacity
    * @return secCap
    */
	public int getSecCap() {
		return secCap;
	}
    
    /**
    * Sets the section capacity to the specified value
    * @param secCap the new section capacity
    */
	public void setSecCap(int secCap) {
		this.secCap = secCap;
	}
    
    /**
    * Gets the course being offered
    * @return theCourse
    */
	public Course getTheCourse() {
		return theCourse;
	}
    
    /**
    * Sets the course being offered to the specified value
    * @param theCourse the new course
    */
	public void setTheCourse(Course theCourse) {
		this.theCourse = theCourse;
	}
	
	/**
	 * Gets the unique offering ID of this course offering
	 * @return the id number
	 */
	public int getOfferingID() {
		return offeringID;
	}

	/**
	 * Sets the unique offeringID for this course offering
	 * @param offeringID is the id number
	 */
	public void setOfferingID(int offeringID) {
		this.offeringID = offeringID;
	}

	@Override
    /**
     * Creates a String representation of a CourseOffering object
     * @return the String representation
     */
	public String toString () {
		String st = "\n";
		st += getTheCourse().getCourseName() + " " + getTheCourse().getCourseNum() + "\n";
		st += "Section Num: " + getSecNum() + ", section cap: "+ getSecCap() +"\n";

		return st; 
	}
	
	/**
	 * Adds a registration to the offering
	 * @author Hannah MacDonald
	 * @param registration is the registration
	 */
	public void addRegistration(Registration registration) {
		offeringRegList.add(registration);
	}
	
	/**
	 * Gets the number of registrations
	 * @author Hannah MacDonald
	 * @return the number of registrations in this course offering
	 */
	public int numberofRegistrations() {
		return offeringRegList.size();
	}
	

}

