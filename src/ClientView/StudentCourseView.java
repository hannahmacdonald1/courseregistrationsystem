package ClientView;

import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import javax.swing.*;

/**
 * Contains GUI elements representing the Student Courses view for the
 * Course Registration System.
 * @author Brianna Gwilliam, Hannah MacDonald, Muskan Sarvesh
 * @since April 12, 2020
 * @version 1.0
 */
@SuppressWarnings("serial")
public class StudentCourseView extends JFrame {
    
    //south panel elements
    private JButton popUpAdd;
    private JButton returnToMain;
    
    //center panel elements
    private JTextArea courses;
    private JScrollPane courseDisplay;
    
    /**
     * Creates the window to view student courses, does NOT make it visible
     * @author Brianna Gwilliam
     */
    public StudentCourseView () {
        super("My Courses");
        setSize(600,400);
        
        createButtonPanel();
        createNamePanel(); 
        createTextPanel();
        pack();
    }
    
    
    /**
	 * Creates the text panel and adds it to the frame
	 * @author Hannah MacDonald
	 */
	private void createTextPanel() {
		courses = new JTextArea(17,50);
		courses.setEditable(false);
		courseDisplay = new JScrollPane(courses);
		courseDisplay.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		
		JPanel textPanel = new JPanel();
		textPanel.add(courseDisplay);
		add(textPanel, BorderLayout.CENTER);
	}
    
	/**
	 * Creates the button panel and adds it to the frame
	 */
    private void createButtonPanel() {
    	popUpAdd = new JButton("Show / Update My Courses");
        returnToMain = new JButton("Return to Main Menu");
        JPanel buttonPanel = new JPanel();
        buttonPanel.add(popUpAdd);
        buttonPanel.add(returnToMain);
        add(buttonPanel, BorderLayout.SOUTH);
    }
    
    /**
     * Creates the name panel and adds it to the frame
     */
    private void createNamePanel() {
       JLabel title = new JLabel("Show Student Courses");
       JPanel namePanel = new JPanel();
       namePanel.add(title);
       add(namePanel, BorderLayout.NORTH);
    }
    
    /**
     * Adds a listener to the "My Courses" button in the student course viewing window
     * @author Muskan Sarvesh
     * @param listenForPopUpAddButton is the action listener
     */
   public void addStudentCourseListener (ActionListener listenForPopUpAddButton) {
        popUpAdd.addActionListener(listenForPopUpAddButton);
   }
    
    
    /**
     * Adds a listener to the "Return to Main Window" button in the student course viewing window
     * @author Brianna Gwilliam
     * @param listenForReturnToMainButton is the action listener
     */
    public void addReturnToMainListener (ActionListener listenForReturnToMainButton) {
            returnToMain.addActionListener(listenForReturnToMainButton);
    }
    
    
    /**
	 * Sets the text in the main text field in the text panel.
	 * @param c is the String to put in the text field
	 * @author Hannah MacDonald
	 */
    public void setCourses(String c) {
    	courses.setText(c);
    }
    
    /**
     * Stops displaying the window to view student courses
     *@author Brianna Gwilliam
     */
    public void closeStudentCourseWindow() {
        setVisible(false);
    }
    
    /**
     * Displays window for viewing student courses
     * @author Brianna Gwilliam
    */
       public void showStudentCourseWindow() {
           setVisible(true);
       }

	/**
	 * Displays an error dialog.
	 * @param errorMessage is the error message.
	 */
    public void displayErrorMessage (String errorMessage) {
        JOptionPane.showMessageDialog(this, errorMessage);
    }


}
