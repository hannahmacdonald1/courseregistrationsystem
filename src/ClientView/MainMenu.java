package ClientView;

import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import javax.swing.*;

/**
 * Contains GUI elements for the Client Side of the Registration System
 * @author Brianna Gwilliam, Hannah MacDonald, Muskan Sarvesh
 * @since April 12, 2020
 * @version 1.0
 */
@SuppressWarnings("serial")
public class MainMenu extends JFrame {
	// North panel elements
	private JLabel title;
	
	// South panel elements
	private JButton addCourse;
	private JButton deleteCourse;
	private JButton viewCatalogue;
	private JButton myCourse;
	private JButton searchCourse;
	
	private AddCourseView adding = new AddCourseView();
	private RemoveCourseView removing = new RemoveCourseView();
	private	CourseCatView catalogue = new CourseCatView();
	private StudentCourseView myCourses = new StudentCourseView();
	private SearchCourseView searching = new SearchCourseView();
	
	/**
	 * Creates the main window, does NOT make it visible
	 * @author Muskan Sarvesh
	 */
	public MainMenu () {
		super("Main Window");
		setSize(450,150);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		createRecordPanel();
		createNamePanel();
		createButtonPanel();
	}
	
	/**
	 * Creates the record panel and adds it to the frame
	 * @author Muskan Sarvesh
	 */
	private void createRecordPanel() {
		JPanel recordPanel = new JPanel();
		add(recordPanel, BorderLayout.CENTER);
	}
	
	/**
	 * Creates the name panel and adds it to the frame
	 * @author Muskan Sarvesh
	 */
	private void createNamePanel() {
		title = new JLabel("Student Registration System");
		
		JPanel namePanel = new JPanel();
		namePanel.add(title);
		add(namePanel, BorderLayout.NORTH);
	}
	
	/**
	 * Creates the button panel and adds it to the frame
	 * @author Muskan Sarvesh
	 */
	private void createButtonPanel() {
		addCourse = new JButton("Add a Course");
		deleteCourse = new JButton("Delete a Course");
		viewCatalogue = new JButton("View all Courses");
        myCourse = new JButton("View Student Courses");
        searchCourse = new JButton("Search for a Course");
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.add(addCourse);
		buttonPanel.add(deleteCourse);
		buttonPanel.add(viewCatalogue);
		buttonPanel.add(myCourse);
		buttonPanel.add(searchCourse);
		add(buttonPanel, BorderLayout.CENTER);
	}
	
	/**
	 * Displays an error dialog.
	 * @param errorMessage is the error message.
	 * @author Muskan Sarvesh
	 */
	public void displayErrorMessage (String errorMessage) {
		JOptionPane.showMessageDialog(this,errorMessage);
	}
		
	/**
	 * Adds a listener to the "Add Course" button
	 * @author Muskan Sarvesh
	 * @param listenForAddButton is the action listener
	 */
	public void addCourseListener (ActionListener listenForAddButton) {
		addCourse.addActionListener(listenForAddButton);
	}
	
	/**
	 * Adds a listener to the "Remove Course" button
	 * @author Muskan Sarvesh
	 * @param listenForRemoveButton is the action listener
	 */
	public void removeCourseListener (ActionListener listenForRemoveButton) {
		deleteCourse.addActionListener(listenForRemoveButton);
	}
	
	/**
	 * Adds a listener to the "Search Course" button
	 * @author Muskan Sarvesh
	 * @param listenForFindButton is the action listener
	 */
	public void searchCourseListener(ActionListener listenForFindButton) {
		searchCourse.addActionListener(listenForFindButton);
	}
	
	/**
	 * Adds a listener to the "My Courses" button
	 * @author Muskan Sarvesh
	 * @param listenForCourseButton is the action listener
	 */
	public void studentCourseListener(ActionListener listenForCourseButton) {
		myCourse.addActionListener(listenForCourseButton);
	}
	
	/**
	 * Adds a listener to the "Catalogue" button
	 * @author Muskan Sarvesh
	 * @param listenForCatalogueButton is the action listener
	 */
	public void courseCatListener(ActionListener listenForCatalogueButton) {
		viewCatalogue.addActionListener(listenForCatalogueButton);
	}

	/*
	 * Getters and setters
	 */
	
    /**
     * Gets the JFrame for the course addition window
     * @return adding
     * @author Muskan Sarvesh
     */
	public AddCourseView getAdding() {
		return adding;
	}

    /**
    * Gets the JFrame for the course removal window
    * @return removing
    * @author Muskan Sarvesh
    */
	public RemoveCourseView getRemoving() {
		return removing;
	}

    /**
    * Gets the JFrame for the course catalogue window
    * @return catalogue
    * @author Muskan Sarvesh
    */
	public CourseCatView getCatalogue() {
		return catalogue;
	}

    /**
    * Gets the JFrame for the student courses  window
    * @return myCourses
    * @author Muskan Sarvesh
    */
	public StudentCourseView getMyCourses() {
		return myCourses;
	}

    /**
    * Gets the JFrame for the course search window
    * @return searching
    * @author Muskan Sarvesh
    */
	public SearchCourseView getSearching() {
		return searching;
	}

}
