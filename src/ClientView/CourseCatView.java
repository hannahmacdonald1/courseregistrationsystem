package ClientView;

import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import javax.swing.*;

/**
 * Contains GUI elements for representing the view of Course Catalogue
 * @author Brianna Gwilliam, Hannah MacDonald, Muskan Sarvesh
 * @since April 12, 2020
 * @version 1.0
 */
@SuppressWarnings("serial")
public class CourseCatView extends JFrame {
    
    //south panel elements
    private JButton catalogue;
    private JButton returnToMain;
    
    //center panel elements
    private JTextArea courses;
    private JScrollPane courseDisplay;
    
    /**
     * Creates the course catalogue window, does NOT make it visible
     * @author Muskan Sarvesh
     */
    public CourseCatView () {
        super("Course Catalogue");
        setSize(600,400);
        
        createButtonPanel();
        createNamePanel();
        createTextPanel();
    
    }
    
	/**
	 * Creates the text panel and adds it to the frame
	 * @author Hannah MacDonald
	 */
	private void createTextPanel() {
		courses = new JTextArea(17,50);
		courses.setEditable(false);
		courseDisplay = new JScrollPane(courses);
		courseDisplay.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		
		JPanel textPanel = new JPanel();
		textPanel.add(courseDisplay);
		add(textPanel, BorderLayout.CENTER);
	}
    
    /**
     * Creates a panel for the buttons for "Show Catalogue" or
     * "Return to Main Menu".
     * @author Muskan Sarvesh
     */
    private void createButtonPanel() {
    	catalogue = new JButton("Show Catalogue");
        returnToMain = new JButton("Return to Main Menu");
        JPanel buttonPanel = new JPanel();
        buttonPanel.add(catalogue);
        buttonPanel.add(returnToMain);
        add(buttonPanel, BorderLayout.SOUTH);
    }
    
    /**
     * Creates a panel for Course Catalogue
     * @author Muskan Sarvesh
     */
    private void createNamePanel() {
       JLabel title = new JLabel("Course Catalogue");
       JPanel namePanel = new JPanel();
       namePanel.add(title);
       add(namePanel, BorderLayout.NORTH);
    }
    
    /**
	 * Sets the text in the main text field in the text panel.
	 * @param c is the String to put in the text field
	 * @author Hannah MacDonald
	 */
    public void setCourses(String c) {
    	courses.setText(c);
    }
    
    /**
     * Adds a listener to the "Show Catalogue" button in the record insertion window
     * @author Muskan Sarvesh
     * @param listenForPopUpAddButton is the action listener
     */
   public void addCatalogueListener (ActionListener listenForPopUpAddButton) {
        catalogue.addActionListener(listenForPopUpAddButton);
   }
    
    
    /**
      * Adds a listener to the "Return to Main Window" button in the record insertion window
      * @author Muskan Sarvesh
      * @param listenForReturnToMainButton is the action listener
       */
       public void addReturnToMainListener (ActionListener listenForReturnToMainButton) {
            returnToMain.addActionListener(listenForReturnToMainButton);
    } 
    
    /**
     * Stops displaying the window to view the course catalogue
     *@author Muskan Sarvesh
     */
    public void closeCatalogueWindow() {
        setVisible(false);
    }
    
    /**
     * Displays window for viewing the course catalogue
     * @author Muskan Sarvesh
    */
       public void showCatalogueWindow() {
           setVisible(true);
       }

	/**
	 * Displays an error dialog.
	 * @param errorMessage is the error message.
	 * @author Muskan Sarvesh
	 */
    public void displayErrorMessage (String errorMessage) {
        JOptionPane.showMessageDialog(this, errorMessage);
    }


}
