package ClientView;

import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import javax.swing.*;

/**
 * Contains GUI elements for the Menu representing the view of Removing Course
 * @author Brianna Gwilliam, Hannah MacDonald, Muskan Sarvesh
 * @since April 12, 2020
 * @version 1.0
 */
@SuppressWarnings("serial")
public class RemoveCourseView extends JFrame {
    
    //south panel elements
    private JButton remove;
    private JButton returnToMain;
    
    //center panel elements
    private JTextField courseNum = new JTextField(10);
    private JTextField courseName = new JTextField(10);

    private JLabel cNumLabel = new JLabel("Enter the Course Number");
    private JLabel cNameLabel = new JLabel("Enter the Course Name");
    
    
    /**
     * Creates the course removal window, does NOT make it visible
     * @author Muskan Sarvesh
     */
    public RemoveCourseView () {
        super("Remove a Course");
        setSize(400,180);
        
        createButtonPanel();
        createNamePanel();
        createInputPanel();
    
    }
    
    /**
     * Creates a panel for showing the button for either removing or
     * returning to the main menu
     * @author Muskan Sarvesh
     */
    private void createButtonPanel() {
    	remove = new JButton("Remove");
        returnToMain = new JButton("Return to Main Menu");
        JPanel buttonPanel = new JPanel();
        buttonPanel.add(remove);
        buttonPanel.add(returnToMain);
        add(buttonPanel, BorderLayout.SOUTH);
    }
    
    /**
     * Creates a panel for showing the removing the courses.
     * @author Muskan Sarvesh
     */
    private void createNamePanel() {
       JLabel title = new JLabel("Remove a Course");
       JPanel namePanel = new JPanel();
       namePanel.add(title);
       add(namePanel, BorderLayout.NORTH);
    }
    
    /**
     * Creates a panel to prompt user input for the course Name and Number
     * @author Muskan Sarvesh
     */
    private void createInputPanel() {
       JPanel inputPanel = new JPanel();
       inputPanel.add(cNameLabel);
       inputPanel.add(courseName);
       inputPanel.add(cNumLabel);
       inputPanel.add(courseNum);
       add(inputPanel, BorderLayout.CENTER);
    }
    
    
    /**
     * Adds a listener to the "Remove Course" button in the course removal window
     * @author Muskan Sarvesh
     * @param listenForPopUpAddButton is the action listener
     */
   public void addRemoveListener (ActionListener listenForPopUpAddButton) {
        remove.addActionListener(listenForPopUpAddButton);
   }
    
    
    /**
     * Adds a listener to the "Return to Main Window" button in the course removal window
     * @author Muskan Sarvesh
     * @param listenForReturnToMainButton is the action listener
     */
       public void addReturnToMainListener (ActionListener listenForReturnToMainButton) {
            returnToMain.addActionListener(listenForReturnToMainButton);
    }
    
    
    /**
     * Gets the Course number from the text field
     *@author Muskan Sarvesh
     *@return the Course Number
     */
    public int getCourseNum() throws NumberFormatException {
        return Integer.parseInt(courseNum.getText());
    }
    
    
    /**
     * Gets the Course Name from the text field
     *@author Muskan Sarvesh
     *@return the Course Name 
     */
    public String getCourseName() {
        return courseName.getText();
    }
    
    
    /**
     * Stops displaying the window to remove a course from a student's registrations
     *@author Muskan Sarvesh
     */
    public void closeRemoveWindow() {
        setVisible(false);
    }
    
    /**
     * Displays window for removing a course from a student's registrations
     * @author Muskan Sarvesh
    */
       public void showRemoveWindow() {
           setVisible(true);
       }

       /**
        * Displays a message when a course was removed successfully.
        * @author Hannah MacDonald
        */
       public void removed() {
      	 JOptionPane.showMessageDialog(null, "The course was removed sucessfully.", "Success!", JOptionPane.INFORMATION_MESSAGE);
       }
    

   /**
    * Displays an error dialog.
    * @param errorMessage is the error message.
    * @author Muskan Sarvesh
    */
    public void displayErrorMessage (String errorMessage) {
        JOptionPane.showMessageDialog(this, errorMessage);
    }
}
