package ClientView;

import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import javax.swing.*;

/**
 * Contains GUI elements for the Menu representing the view of Adding Courses
 * @author Brianna Gwilliam, Hannah MacDonald, Muskan Sarvesh
 * @since April 12, 2020
 * @version 1.0
 */
@SuppressWarnings("serial")
public class AddCourseView extends JFrame {
    
    //south panel elements
    private JButton popUpAdd;
    private JButton returnToMain;
    
    //center panel elements
    private JTextField courseNum = new JTextField(10);
    private JTextField courseName = new JTextField(10);
    private JTextField courseSec = new JTextField(10);

    private JLabel cNumLabel = new JLabel("Enter the Course Number");
    private JLabel cNameLabel = new JLabel("Enter the Course Name");
    private JLabel cSecLabel = new JLabel("Enter the Course Section Number");
    
    
    /**
     * Creates the course addition window, does NOT make it visible
     * @author Muskan Sarvesh
     */
    public AddCourseView () {
        super("Add a Course");
        setSize(400,200);
        
        createButtonPanel();
        createNamePanel();
        createInputPanel();
        //pack();
    
    }
    
    /**
     * Creates a panel for showing the button for either adding or
     * returning to the main menu
     * @author Muskan Sarvesh
     */
    private void createButtonPanel() {
    	popUpAdd = new JButton("Add");
        returnToMain = new JButton("Return to Main Menu");
        JPanel buttonPanel = new JPanel();
        buttonPanel.add(popUpAdd);
        buttonPanel.add(returnToMain);
        add(buttonPanel, BorderLayout.SOUTH);
    }
    
    /**
     * Creates a panel for showing the Add Courses Panel
     * @author Muskan Sarvesh
     */
    private void createNamePanel() {
       JLabel title = new JLabel("Add a New Course");
       JPanel namePanel = new JPanel();
       namePanel.add(title);
       add(namePanel, BorderLayout.NORTH);
    }
    
    /**
     * Creates a panel which prompts the user to enter the 
     * Course Name and Number
     * @author Muskan Sarvesh
     */
    private void createInputPanel() {
       JPanel inputPanel = new JPanel();
       inputPanel.add(cNameLabel);
       inputPanel.add(courseName);
       inputPanel.add(cNumLabel);
       inputPanel.add(courseNum);
       inputPanel.add(cSecLabel);
       inputPanel.add(courseSec);
       add(inputPanel, BorderLayout.CENTER);
    }
    
    
    /**
     * Adds a listener to the "Adding" button in the course addition window
     * @author Muskan Sarvesh
     * @param listenForPopUpAddButton is the action listener
     */
   public void addPopUpAddListener (ActionListener listenForPopUpAddButton) {
        popUpAdd.addActionListener(listenForPopUpAddButton);
   }
    
    
    /**
     * Adds a listener to the "Return to Main Window" button in the course addition window
     * @author Brianna Gwilliam
     * @param listenForReturnToMainButton is the action listener
     */
     public void addReturnToMainListener (ActionListener listenForReturnToMainButton) {
            returnToMain.addActionListener(listenForReturnToMainButton);
    }
    
    
    /**
     * Gets the course number from the associated text field
     *@author Hannah MacDonald
     *@return the course number
     */
    public int getCourseNum() throws NumberFormatException {
        return Integer.parseInt(courseNum.getText());
    }
    
    /**
     * Gets the course section number from the associated text field
     *@author Hannah MacDonald
     *@return the course section number
     */
    public int getCourseSec() throws NumberFormatException {
        return Integer.parseInt(courseSec.getText());
    }
    
    /**
     * Gets the course name from the associated text field
     *@author Brianna Gwilliam
     *@return the faculty
     */
    public String getCourseName() {
        return courseName.getText();
    }
    
    
    /**
     * Stops displaying the window to add a new course to a student's courses
     *@author Brianna Gwilliam
     */
    public void closeAddWindow() {
        setVisible(false);
    }
    
    /**
     * Displays window for adding a new course to a student's courses
     * @author Brianna Gwilliam
    */
       public void showAddWindow() {
           setVisible(true);
       }
       
       /**
        * Displays a message when a course was added successfully.
        * @author Hannah MacDonald
        */
       public void added() {
      	 JOptionPane.showMessageDialog(null, "The course was added sucessfully.", "Success!", JOptionPane.INFORMATION_MESSAGE);
       }
    

	/**
	 * Displays an error dialog.
	 * @param errorMessage is the error message.
	 */
    public void displayErrorMessage (String errorMessage) {
        JOptionPane.showMessageDialog(this, errorMessage);
    }


}
