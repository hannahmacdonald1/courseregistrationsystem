package ClientView;

import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import javax.swing.*;

/**
 * Contains GUI elements for representing the Search view for the 
 * Course Registration System 
 * @author Brianna Gwilliam, Hannah MacDonald, Muskan Sarvesh
 * @since April 12, 2020
 * @version 1.0
 */
@SuppressWarnings("serial")
public class SearchCourseView extends JFrame {
    
    //south panel elements
    private JButton search;
    private JButton returnToMain;
    
    //center panel elements
    private JTextField courseNum = new JTextField(10);
    private JTextField courseName = new JTextField(10);

    private JLabel cNumLabel = new JLabel("Enter the Course Number");
    private JLabel cNameLabel = new JLabel("Enter the Course Name");
    
    
    /**
     * Creates the course search window, does NOT make it visible
     * @author Muskan Sarvesh
     */
    public SearchCourseView () {
        super("Search");
        setSize(400,180);
        
        createButtonPanel();
        createNamePanel();
        createInputPanel();
    
    }
    
    /**
     * Creates the panel for Buttons either to "Search" or 
     * to "Return to Main Menu"
     * @author Muskan Sarvesh
     */
    private void createButtonPanel() {
    	search = new JButton("Search");
        returnToMain = new JButton("Return to Main Menu");
        JPanel buttonPanel = new JPanel();
        buttonPanel.add(search);
        buttonPanel.add(returnToMain);
        add(buttonPanel, BorderLayout.SOUTH);
    }
    
    /**
     * Creates the panel for "Search for a Course"
     * @author Muskan Sarvesh
     */
    private void createNamePanel() {
       JLabel title = new JLabel("Search for a Course");
       JPanel namePanel = new JPanel();
       namePanel.add(title);
       add(namePanel, BorderLayout.NORTH);
    }
    
    /**
     * Creates a panel for user input for Course Number and Name
     * @author Muskan Sarvesh
     */
    private void createInputPanel() {
       JPanel inputPanel = new JPanel();
       inputPanel.add(cNameLabel);
       inputPanel.add(courseName);
       inputPanel.add(cNumLabel);
       inputPanel.add(courseNum);
       add(inputPanel, BorderLayout.CENTER);
    }
    
    
    /**
     * Adds a listener to the "Search" button in the course search window
     * @author Muskan Sarvesh
     * @param listenForPopUpAddButton is the action listener
     */
   public void addSearchListener (ActionListener listenForPopUpAddButton) {
        search.addActionListener(listenForPopUpAddButton);
   }
    
    
    /**
     * Adds a listener to the "Return to Main Window" button in the course search window
     * @author Muskan Sarvesh
     * @param listenForReturnToMainButton is the action listener
     */
     public void addReturnToMainListener (ActionListener listenForReturnToMainButton) {
            returnToMain.addActionListener(listenForReturnToMainButton);
    }
    
    
    /**
     * Gets the Course number from the text field
     *@author Muskan Sarvesh
     *@return the Course Number
     */
    public int getCourseNum() throws NumberFormatException {
        return Integer.parseInt(courseNum.getText());
    }
    
    /**
     * Gets the Course Name from the text field
     *@author Muskan Sarvesh
     *@return the Course Name
     */
    public String getCourseName() {
        return courseName.getText();
    }
    
    
    /**
     * Stops displaying the window to search for a course in the course catalogue
     *@author Muskan Sarvesh
     */
    public void closeSearchWindow() {
        setVisible(false);
    }
    
    /**
     * Displays window for searching for a course in the course catalogue
     * @author Muskan Sarvesh
    */
     public void showSearchWindow() {
           setVisible(true);
     }
     
     /**
      * Displays the results of a successful search
      * @param course is the string version of the course information.
      */
     public void searchResults(String course) {
    	 String message = "The following course and course section(s) was/were found:\n" + course;
    	 JOptionPane.showMessageDialog(null, message, "Course was found!", JOptionPane.INFORMATION_MESSAGE);
     }
    

     /**
      * Displays an error dialog.
      * @param errorMessage is the error message.
      */
    public void displayErrorMessage (String errorMessage) {
        JOptionPane.showMessageDialog(this, errorMessage);
    }


}
