package clientController;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import ClientView.MainMenu;


/**
 * A class that represents a client that communicates with the server in a client/server
 * application. 
 * 
 * @author Brianna Gwilliam, Hannah MacDonald, Muskan Sarvesh
 * @version 1.0
 * @since April 12, 2020
 *
 */
public class ClientComController { 
	
	/**
	 * Used for writing to the socket
	 */
	private PrintWriter socketOut;
	
	/**
	 * The socket used by the client and server
	 */
	private Socket aSocket;
	
	/**
	 * Used for reading from the socket
	 */
	private BufferedReader socketIn;
	
	
	/**
     * Used to display the server's response to the GUI
     */
	private GUIController gui;
	

	/**
	 * Constructs a client with the given host name and port. 
	 * @param serverName the host name
	 * @param portNumber the port number.
	 * @param menu is the main menu for the GUI
     * @author Brianna Gwilliam
	 */
	public ClientComController(String serverName, int portNumber, MainMenu menu) {
		try {
			aSocket = new Socket(serverName, portNumber);
			socketIn = new BufferedReader(new InputStreamReader(aSocket.getInputStream()));
			socketOut = new PrintWriter(aSocket.getOutputStream(), true);
			
			gui = new GUIController(menu, this);
		} 
		
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
    /**
     * Gets the object responible for writing to the socket
     * @return socketOut
     * @author Brianna Gwilliam
    */
	public PrintWriter getSocketOut() {
		return socketOut;
	}
	
	/**
    * Gets the object responible for reading from the socket
    *@return socketIn
     * @author Brianna Gwilliam
    */
	public BufferedReader getSocketIn() {
		return socketIn;
	}
	
	
	/**
	 * Runs the client. 
	 * @param args is void
	 * @throws IOException
     * @author Brianna Gwilliam
	 */
	public static void main(String[] args) throws IOException {
		MainMenu menu = new MainMenu();
		ClientComController client = new ClientComController("localhost", 8099, menu);
	}
	

}


