package clientController;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import javax.swing.JOptionPane;

import ClientView.MainMenu;

/**
 * A class that receives user input from the GUI and displays the appropriate response via communication with the server.
 * @author Brianna Gwilliam, Hannah MacDonald, Muskan Sarvesh
 * @version 1.0
 * @since April 12, 2020
 *
 */
public class GUIController {
    
    /**
     * The main menu that will be displayed.
     */
    private MainMenu mainView;
    
    /**
    *Assists with communication with the server.
    */
    private ClientComController com;
    
    /**
    * Used for reading from the socket
    */
    private BufferedReader socketInput;
    
    /**
    * Used for writing to the server
    */
    private PrintWriter socketOutput;
    
    /**
     * Contains the studentID for the current student
     */
    private int studentID;
    
    /**
     * Constructs a GUIController with the specified values for member variables mainView and com. Also prompts user
     * to enter the student's information then makes the main menu visible.
     * @param mainView the value for mainView
     * @param com the value for com
     * @author Hannah MacDonald
    */
    public GUIController(MainMenu mainView, ClientComController com) {
        this.mainView = mainView;
        this.com = com;
        
        // Gets the student information
        String user = getStudent();
        while (user == null) {
            user = getStudent();
        }
        
        com.getSocketOut().println(user);
        
        mainView.addCourseListener(new addCourseListener());
        mainView.removeCourseListener(new removeCourseListener());
        mainView.searchCourseListener(new searchListener());
        mainView.studentCourseListener(new studentCourseListener());
        mainView.courseCatListener(new courseCatListener());
        mainView.addWindowListener(new WindowEventHandler());
        mainView.setVisible(true);
    }
    
    /*
     * Add Course menu functions
     */
    /**
     * Listener for the "Add a Course" button
     * @author Hannah MacDonald
     */
    private class addCourseListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            mainView.getAdding().showAddWindow();
            mainView.getAdding().addPopUpAddListener(new addButton());
            mainView.getAdding().addReturnToMainListener(new returnToMainMenu());
        }
        private class returnToMainMenu implements ActionListener {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainView.getAdding().closeAddWindow();
            }
        }
    }
    
    /**
     * Listener for the "Add" button in the "Add a Course" menu,
     * adds a course to this student's courses.
     * @author Hannah MacDonald
     */
    private class addButton implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            try {
            String courseName = mainView.getAdding().getCourseName();
            int courseNum = mainView.getAdding().getCourseNum();
            int courseSection = mainView.getAdding().getCourseSec();
            String line = "2," + courseName + "," + courseNum + "," + courseSection + "," + studentID;
            com.getSocketOut().println(line);
            

            String response;
            response = com.getSocketIn().readLine();
                
            
            if(response.equals("offering")) {
                mainView.getAdding().displayErrorMessage("Course section was not found!");
            }
            
            else if(response.equals("course")) {
                mainView.getAdding().displayErrorMessage("Course was not found!");
            }
            
            else if(response.equals("registrations")) {
                mainView.getAdding().displayErrorMessage("Error! This student is enrolled in too many courses!");
            }
            
            else if(response.equals("sectionfull")) {
                mainView.getAdding().displayErrorMessage("Error! The course section is full!");
            }
            
            else if(response.equals("alreadyenrolled")) {
                mainView.getAdding().displayErrorMessage("Error! Student is already enrolled in this course!");
            }
            
            else
                mainView.getAdding().added();
            
            } catch (NumberFormatException exp) {
                mainView.getAdding().displayErrorMessage ("Course number or section number is invalid.");
            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
        }
        
    }
    
    /*
     * Delete a course menu functions
     */
    
    /**
     * Listener for the "Delete a Course" button
     * @author Hannah MacDonald
     */
    private class removeCourseListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            mainView.getRemoving().showRemoveWindow();
            mainView.getRemoving().addRemoveListener(new deleteButton());
            mainView.getRemoving().addReturnToMainListener(new returnToMainMenu());
        }
        private class returnToMainMenu implements ActionListener {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainView.getRemoving().closeRemoveWindow();
            }
        }
    }
    
    /**
     * Listener for the "Delete" button in the "Delete a Course" menu, deletes
     * a course from the student's courses.
     * @author Hannah MacDonald
     */
    private class deleteButton implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                String courseName = mainView.getRemoving().getCourseName();
                int courseNum = mainView.getRemoving().getCourseNum();
                
                String line = "3," + courseName + "," + courseNum;
                com.getSocketOut().println(line);
                
                String response;
                
                    response = com.getSocketIn().readLine();
                    
                    if(response.equals("notenough")) {
                        mainView.getRemoving().displayErrorMessage("Error! Course offering has 8 or fewer registrations.\nThe student cannot be unenrolled.\n");
                    }
                    
                    else if(response.equals("notfound")) {
                        mainView.getRemoving().displayErrorMessage("Error! Course was not found in student's registration list!");
                    }
                    
                    else
                        mainView.getRemoving().removed();
                    
                }
                catch (IOException e1) {
                    e1.printStackTrace();
                }
                    
             catch (NumberFormatException exp) {
                mainView.getRemoving().displayErrorMessage("Course number is invalid.");
            }
            
        }
        
    }
    
    /*
     * Course Catalogue menu functions
     */
    
    /**
     * Listener for the "View all Courses" button
     * @author Hannah MacDonald
     */
    class courseCatListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            mainView.getCatalogue().showCatalogueWindow();
            mainView.getCatalogue().addCatalogueListener(new showCatalogueListener());
            mainView.getCatalogue().addReturnToMainListener(new returnToMainMenu());
        }
        private class returnToMainMenu implements ActionListener {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainView.getCatalogue().closeCatalogueWindow();
            }
        }
        
    }
    
    /**
     * Listener for the "Show Catalogue" button in the "View All Courses" menu,
     * shows the current state of the catalogue
     * @author Hannah MacDonald
     */
    private class showCatalogueListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            
            String line = "4";
            com.getSocketOut().println(line);
            String response ="";
            try {
                while (true) {
                    response += com.getSocketIn().readLine() + "\n";
                    if (response.contains("\0")) {
                        response = response.replace("\0", "");
                        break;
                    }
                }
                mainView.getCatalogue().setCourses(response);
            }
            catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }
    
    /*
     * View Student Courses Menu Functions
     */
    
    /**
     * Listener for the "View Student Courses" button
     * @author Hannah MacDonald
     */
    class studentCourseListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            mainView.getMyCourses().showStudentCourseWindow();
            mainView.getMyCourses().addReturnToMainListener(new returnToMainMenu());
            mainView.getMyCourses().addStudentCourseListener(new showStudentCourseListener());
        }
        private class returnToMainMenu implements ActionListener {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainView.getMyCourses().closeStudentCourseWindow();
            }
        }
    }
    
    /**
    * Listener for the "My Courses" button in the "View Student Courses" menu,
    * shows the student's current courses.
    * @author Hannah MacDonald
    */
    private class showStudentCourseListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            
            String line = "5";
            com.getSocketOut().println(line);
            
            String response ="";
            try {
                while (true) {
                    response += com.getSocketIn().readLine() + "\n";
                    if (response.contains("\0")) {
                        response = response.replace("\0", "");
                        break;
                    }
                }
                mainView.getMyCourses().setCourses(response);
            }
            catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }
    
    /*
     * Search menu functions
     */
    
    /**
     * Listener for the "Search for a Course" button
     * @author Hannah MacDonald
     */
    class searchListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            mainView.getSearching().showSearchWindow();
            mainView.getSearching().addSearchListener(new searchButton());
            mainView.getSearching().addReturnToMainListener(new returnToMainMenu());
        }
        private class returnToMainMenu implements ActionListener {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainView.getSearching().closeSearchWindow();
            }
        }
    }
    
    /**
     * Listener for the "Search" button in the Search menu,
     * performs a search of the catalogue.
     * @author Hannah MacDonald
     */
    private class searchButton implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                String courseName = mainView.getSearching().getCourseName();
                int courseNum = mainView.getSearching().getCourseNum();
                
                String line = "1," + courseName + "," + courseNum;
                com.getSocketOut().println(line);
                
                
                String response;
                response = com.getSocketIn().readLine();
                if(response.contains("-------")) {
                    mainView.getSearching().displayErrorMessage("Course was not found!");
                }
                else {
                    while (true) {
                        response += com.getSocketIn().readLine() + "\n";
                        if (response.contains("-------")) {
                            response = response.replace("-------", "");
                            break;
                        }
                    }
                
                    mainView.getSearching().searchResults(response);
                }
                        
            }
                catch (IOException e1) {
                    e1.printStackTrace();
                }
        
                 catch (NumberFormatException exp) {
                    mainView.getSearching().displayErrorMessage("Course number is invalid.");
                }
        }
    }
    
    
    /**
     * Used for the appropriate termination of the client application
     * upon detection of the main window being closed.
     * @author Brianna Gwilliam
     *
     */
    private class WindowEventHandler extends WindowAdapter {
          public void windowClosing(WindowEvent evt) {
              String line = "QUIT";
              com.getSocketOut().println(line);
          }
        }
    
    
    /**
     * Gets the information to make or retrieve a student from the user
     * @author Hannah MacDonald
     * @return a String with the correct command for the server, or null if the student id is not an integer
     */
    public String getStudent () {
        String st = JOptionPane.showInputDialog("Please enter the student id:");
        int id;
        try {
            id = Integer.parseInt(st);
        } catch (NumberFormatException e) {
            mainView.displayErrorMessage("Invalid student id, please try again.");
            return null;
        }
        String name = JOptionPane.showInputDialog("Please enter the student name:");
        studentID = id;
        return "6," + name + "," + id;
        
    }
    
}



